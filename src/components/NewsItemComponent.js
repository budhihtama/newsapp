import React from 'react';
import {View, Image, Text, StyleSheet} from 'react-native';

export default class NewsItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.itemContainer}>
        <View>
          <Image source={{uri: this.props.image}} style={styles.itemImage} />
        </View>

        <View style={styles.itemContent}>
          <View>
            <Text style={styles.title}>{this.props.title}</Text>
          </View>

          <View>
            <Text>
              {this.props.author} {this.props.date}
            </Text>
          </View>

          <View>
            <Text>{this.props.description}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    padding: 10,
  },
  itemImage: {
    width: 100,
    height: 100,
  },
  itemContent: {
    padding: 10,
    flex: 3,
  },
  title: {
    fontWeight: 'bold',
  },
});
