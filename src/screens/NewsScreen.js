import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import axios from 'axios';
import NewsItem from '../components/NewsItemComponent';
// import './App.css';
// import NewsItem from './NewsItem.Component';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
    };
  }

  componentDidMount() {
    axios({
      method: 'GET', // POST ,PATCH, PUT, DELETE
      url:
        'http://newsapi.org/v2/everything?q=apple&from=2020-10-19&to=2020-10-19&sortBy=popularity&apiKey=ab4a45b41a7b4ae080b6f95e9e2119e6',
    })
      .then((res) => {
        //when success
        console.log('article', res.data.articles);
        this.setState({listNews: res.data.articles});
      })
      .catch((_err) => {
        //when error
      });
  }

  render() {
    return (
      <View>
        <View>
          <Text style={styles.header}>NEWS FOR YOU</Text>
        </View>

        <ScrollView>
          {this.state.listNews.map((news, index) => (
            <NewsItem
              key={index}
              image={news.urlToImage}
              title={news.title}
              author={news.author}
              date={news.date}
              description={news.description}
            />
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
