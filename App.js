/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import NewsScreen from './src/screens/NewsScreen';

function App() {
  // const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <NewsScreen />
      </SafeAreaView>
    </>
  );
}

export default App;
